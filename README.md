# Linux-Flyer
A flyer introducing Linux first created for Linux Presentation Day 2015 in Germany. It's available in German and English in editable format. Both languages are in the same file, just toggle the layers containing 'en' or 'de'.

## Contents

* The file `LinuxPresentationDay-Flyer.svg` is intended to be exported in two halves for double-sided printing on A4 (or it could be used as a poster in A3).

* The file `LinuxPresentationDay-Flyer_Booklet_version_de.svg` can be printed on two A3 pages, after toggling visibility for the layers Seite1 and Seite2 separately and exporting, and then be cut by horizontally connecting the cutting marks, and be assembled into a small 12 page brochure.

## Editing

To view the files correctly, open with Inkscape. You will need the following fonts:  
[Lenka](https://fontlibrary.org/en/font/lenka-stabilo)  
Domestic Manners: [from Debian Archives](http://ftp.de.debian.org/debian-archive/debian/pool/main/t/ttf-dustin/ttf-dustin_20030517.orig.tar.gz) / [from dafont](http://www.dafont.com/domestic-manners.font) / [from github](https://github.com/dustismo)

## Additional info
Created with [Inkscape](https://inkscape.org).

English version low quality png preview (showing version from Mon Jul 10 2017):

![Low quality png preview](images/LinuxPresentationDay-Flyer.png)
